import sys 




class Board():
    

    def __init__(self):
        self.__black = 0
        self.__white = 0

    def Move(self, index, player):
        if player == 1:
            self.__black = self.__black | (1 << index)
        else:
            self.__white = self.__white | (1 << index)

    def Remove(self, index, player):
        if player == 1:
            self.__black = self.__black & ~(1 << index)
        else:
            self.__white = self.__white & ~(1 << index)

    def CheckFive(self, index, player):
        board = self.__black if player == 1 else self.__white
    
        x = index & 0x7
        y = index >> 3
    
        mask1 = 0xFF << (index & 0x38) #橫向
        mask2 = 0x0101010101010101 << x #直向

    

        d = (x - y) << 3
        mask3 = ((0x8040201008040201 >> d) if d >= 0 
                else (0x8040201008040201 << -d) & 0xFFFFFFFFFFFFFFFF)

        d = (x + y - 7) << 3
        mask4 = ((0x0102040810204080 << d) & 0xFFFFFFFFFFFFFFFF if d >= 0 
                else (0x0102040810204080 >> -d))

        magic1 = 0x101010101010101 
        magic2 = 0x102040810204080 >> x

        v1 = (((board & mask1) * magic1) >> 56) & 0xFF
        v2 = (((board & mask2) * magic2) >> 56) & 0xFF
        v3 = (((board & mask3) * magic1) >> 56) & 0xFF
        v4 = (((board & mask4) * magic1) >> 56) & 0xFF

        rows = (v1, v2, v3, v4)

        for v in rows:
            count = 0
            while v != 0:
                count = count + 1
                v = (v >> 1) & v
            if count >= 5:
                return True

        return False

    def GetMoves(self):
        DeBruijn = 0b0000001000101111110111010110001111001100100101010011100001101101
        table = [  0,  1,  2, 53,  3,  7, 54, 27,
                   4, 38, 41,  8, 34, 55, 48, 28,
                  62,  5, 39, 46, 44, 42, 22,  9,
                  24, 35, 59, 56, 49, 18, 29, 11,
                  63, 52,  6, 26, 37, 40, 33, 47,
                  61, 45, 43, 21, 23, 58, 17, 10,
                  51, 25, 36, 32, 60, 20, 57, 16,
                  50, 31, 19, 15, 30, 14, 13, 12]

        position = ~(self.__black | self.__white) & 0xFFFFFFFFFFFFFFFF;
        
        moves = []
        while (position > 0):
            LS1B = position & (-position)
            key = ((LS1B * DeBruijn) >> 58) & 0x3F
            moves.append(table[key])
            #moves.append(LS1B)
            position ^= LS1B

        return moves

    def GetMoves2(self):
        position = ~(self.__black | self.__white) & 0xFFFFFFFFFFFFFFFF;
        
        moves = []
        mask = 1
        for index in range(64):
            if (position & mask) == mask:
                moves.append(index)
            mask <<= 1

        return moves


    def __str__(self):
        s = "   -A-B-C-D-E-F-G-H-\n"
        mask = 1
        for y in range(8):
            s += f"{y:2}│"
            for x in range(8):
                if (self.__black & mask) != 0:
                    s += " O"
                elif (self.__white & mask) != 0:
                    s += " X"
                else:
                    s += " -"
                mask <<= 1
            s += "│\n"

        return s






def TestBoard():
    board = Board()

    board.Move(1, 1)
    board.Move(2, -1)
    board.Move(3, 1)
    board.Move(4, -1)
    board.Move(5, 1)
    print(board)

    board.Remove(5, 1)
    print(board)

def TestFive1():
    board = Board()

    board.Move(1, 1)
    board.Move(2, 1)
    board.Move(3, 1)
    board.Move(4, 1)
    board.Move(5, 1)
    print(board)

    five = board.CheckFive(5, 1)
    assert five
    print(five)

    board.Remove(5, 1)
    print(board)
    five = board.CheckFive(5, 1)
    assert not five

    print(five)


def TestFive2():
    board = Board()

    board.Move(3, -1)
    board.Move(11, -1)
    board.Move(19, -1)
    board.Move(27, -1)
    board.Move(35, -1)
    print(board)

    five = board.CheckFive(35, -1)
    assert five
    print(five)

    board.Remove(35, -1)
    print(board)
    five = board.CheckFive(5, 1)
    assert not five
    print(five)


def TestFive3():
    board = Board()

    board.Move(15, -1)
    board.Move(22, -1)
    board.Move(29, -1)
    board.Move(36, -1)
    board.Move(43, -1)
    print(board)

    five = board.CheckFive(43, -1)
    assert five
    print(five)

    board.Remove(43, -1)
    print(board)
    five = board.CheckFive(43, 1)
    assert not five
    print(five)

def TestFive4():
    board = Board()

    board.Move(11, -1)
    board.Move(20, -1)
    board.Move(29, -1)
    board.Move(38, -1)
    board.Move(47, -1)
    print(board)

    five = board.CheckFive(47, -1)
    assert five
    print(five)

    board.Remove(43, -1)
    print(board)
    five = board.CheckFive(47, 1)
    assert not five
    print(five)

def TestMoves():
    board = Board()

    board.Move(1, -1)
    board.Move(3, -1)
    board.Move(5, -1)
    board.Move(7, -1)
    board.Move(9, -1)
    board.Move(11, -1)

    moves = board.GetMoves()
    
    print(moves)


if __name__ == "__main__":
    TestBoard()
    TestFive1()
    TestFive2()
    TestFive3()
    TestFive4()

    TestMoves()